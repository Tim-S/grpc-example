# Welcome !
This example demonstrates demonstrates how one can use protobuf in a cmake-project

# Remote Debug via GDB/gdbserver
The project features a [docker-compose.yml](https://gitlab.com/Tim-S/cpprestserversendeventsserver-example/blob/master/docker-compose.yml) 
that can be used to
[Remote Debug and Develop in CLion](https://www.jetbrains.com/help/clion/remote-projects-support.html).

Build the remote-development environment with
```bash
docker-compose build
```

Start the service via
```bash
docker-compose up
```
and connect to the container using credentials ssh://debugger@localhost:7776 and password _"pwd"_