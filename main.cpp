#include <iostream>
#include <sys/time.h>
#include <google/protobuf/util/json_util.h>

#include "Track.pb.h"

using namespace std;

int main() {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    io::radar::WGS84Position sensorPosition;
    sensorPosition.set_latitude(48.392056);
    sensorPosition.set_longitude(9.972077);
    sensorPosition.set_altitude(1.5);

    io::radar::Track track;
    track.set_id(1);
    *track.mutable_sensorposition() = sensorPosition;

    struct timeval tv{};
    gettimeofday(&tv, nullptr);
    track.mutable_time()->set_seconds(tv.tv_sec);
    track.mutable_time()->set_nanos(tv.tv_usec * 1000);

    track.set_slantrange(1);
    track.set_azimuth(2);
    track.set_elevation(3);

    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;
    options.always_print_primitive_fields = true;
    options.preserve_proto_field_names = true;

    string jsonString;
    google::protobuf::util::MessageToJsonString(track, &jsonString, options);
    cout << jsonString << endl;

    // Optional: Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}